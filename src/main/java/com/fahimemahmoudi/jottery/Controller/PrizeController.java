package com.fahimemahmoudi.jottery.Controller;

import com.fahimemahmoudi.jottery.Model.entities.Prize;
import com.fahimemahmoudi.jottery.Model.services.LevelService;
import com.fahimemahmoudi.jottery.Model.services.PrizeService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Route("prizes")
//@UIScope
public class PrizeController extends VerticalLayout {

    LevelService levelService;
    PrizeService prizeService;

    @Autowired
    public PrizeController(LevelService levelService,
                           PrizeService prizeService) {

        this.levelService = levelService;
        this.prizeService = prizeService;

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Label labelName = new Label("Prize name");
        TextField textName = new TextField();

        Label labelCount = new Label("Prize count");
        TextField lc = new TextField();

        Label labelLevel = new Label("Prize level");

        ComboBox comboBox = new ComboBox();
        List<String> items = levelService.findAllClientLeveNames();
        comboBox.setItems(items);

        Button addLevel = new Button("Add", event -> {
            prizeService.addNewPrize(textName.getValue(), Integer.valueOf(lc.getValue()),
                                            comboBox.getValue());
        });

        horizontalLayout.add(labelName, textName, labelCount, lc, labelLevel, comboBox, addLevel);

        Grid<Prize> gridTable = new Grid<>(Prize.class);
        gridTable.setHeightByRows(true);

        Button process = new Button("Process", event ->{
            gridTable.getDataProvider().refreshAll();
            gridTable.setDataProvider(DataProvider.ofCollection(prizeService.findAllPrizes()));
            }
        );

        add(new H1("adding prize"), horizontalLayout, process, gridTable);
    }
}
