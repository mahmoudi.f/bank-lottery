package com.fahimemahmoudi.jottery.Controller;

import com.fahimemahmoudi.jottery.Model.entities.Level;
import com.fahimemahmoudi.jottery.Model.services.LevelService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;


@Route("levels")
//@UIScope
public class LevelController extends VerticalLayout {

    LevelService levelService;

    @Autowired
    public LevelController(LevelService levelService) {

        this.levelService = levelService;

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Label labelName = new Label("Enter Level name");
        TextField textName = new TextField();

        Label labelStart = new Label("Enter start of range");
        TextField ls = new TextField();

        Label labelEnd = new Label("Enter end of range");
        TextField le = new TextField();

        Button addLevel = new Button("Add", event -> {
            try {
                levelService.addNewLevel(new Level(textName.getValue(), Integer.valueOf(ls.getValue()),
                        Integer.valueOf(le.getValue()), 0));
                //clear all fields
                textName.clear();
                ls.clear();
                le.clear();

            } catch (NumberFormatException e) {
                e.printStackTrace();
                //give notiffffff
            }
        }
    );

        horizontalLayout.add(labelName, textName, labelStart, ls, labelEnd, le, addLevel);

        Grid<Level> gridTable = new Grid<>(Level.class);
        gridTable.setHeightByRows(true);
        gridTable.setColumns(new String[]{"levelName", "rangeStart", "rangeEnd", "numberOfClients"});

        Button process = new Button("Process", event ->{
            gridTable.getDataProvider().refreshAll();
            gridTable.setDataProvider(DataProvider.ofCollection(levelService.findAllLevels()));
            }
        );

        add(new H1("adding client level"), horizontalLayout, process, gridTable);
        }
}
