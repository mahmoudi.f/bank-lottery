package com.fahimemahmoudi.jottery.Controller;

import com.fahimemahmoudi.jottery.Model.entities.Lottery;
import com.fahimemahmoudi.jottery.Model.services.LotteryService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route("lotteries")
//@UIScope
public class LotteryController extends VerticalLayout {

    LotteryService lotteryService;

    @Autowired
    public LotteryController (LotteryService lotteryService) {

        this.lotteryService = lotteryService;

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Button addLottery = new Button("Add", event -> {
            lotteryService.addNewLottery();
            }
        );

        horizontalLayout.add(addLottery);

        Grid<Lottery> gridTable = new Grid<>(Lottery.class);
        gridTable.setHeightByRows(true);
//        gridTable.setColumns("level_name", "range_start","range_end", "number_of_clients");

        Button process = new Button("Process", event ->{
            gridTable.setDataProvider(DataProvider.ofCollection(
                    lotteryService.findAllLotteries()));
        }
        );

        add(new H1("adding lottery"), horizontalLayout, process, gridTable);
    }

}
