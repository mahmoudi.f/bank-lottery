package com.fahimemahmoudi.jottery.Model.others;


public enum Gender {
    FEMALE,
    MALE,
    OTHER,
    COMPLICATED
}
