package com.fahimemahmoudi.jottery.Model.services;

import com.fahimemahmoudi.jottery.Model.entities.Client;
import com.fahimemahmoudi.jottery.Model.entities.Level;
import com.fahimemahmoudi.jottery.Model.others.Gender;
import com.fahimemahmoudi.jottery.Repository.ClientRepository;
import com.fahimemahmoudi.jottery.Repository.LevelRepository;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

@Service @NoArgsConstructor
public class ClientImportService {

    @Value("${clientList.path}")
    private String ClientListPath;
    private ClientRepository clientRepo;
    private ClientValidator clientValidator;
    private LevelRepository levelRepo;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    Scanner scanner;

    @Autowired
    public ClientImportService(ClientRepository clientRepo, ClientValidator clientValidator,
                               LevelRepository levelRepo) {

        this.clientRepo = clientRepo;
        this.clientValidator = clientValidator;
        this.levelRepo = levelRepo;
    }

    public void init(){
        try {
            scanner = new Scanner(new File(ClientListPath));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean hasNext() {
        return (scanner.hasNext());
    }

    @SneakyThrows
    public Client addClient() {

         String[] nextRecord;
         Client client = new Client();
         nextRecord = scanner.nextLine().split(",");

        try {
            client = new Client(nextRecord[0], nextRecord[1], nextRecord[2], nextRecord[3],
                    Integer.valueOf(nextRecord[4]), sdf.parse(nextRecord[5]),
                    Gender.valueOf(nextRecord[6].toUpperCase()), nextRecord[7]);

            if (clientValidator.IsValidRecord(client)) {
//              Long levelId = levelRepo.getClientLevelIdByRange(client.getRate());
                Level l = levelRepo.findByRangeEndAfterAndRangeStartBefore(client.getRate(), client.getRate());
//                Level cl = levelRepo.getClientLevelById(l.getLevelId());
                client.setClientlevel(l);
                addClientToRepo(client, l);
            }
        }
        catch (ParseException e) {
            System.out.println("here1:   "+e.getMessage());
            //parse date exception
        }
        catch (IllegalArgumentException | NullPointerException e) {
            System.out.println("here2:   "+e.getMessage());
            //enum exceptions
        }
        catch (TransactionSystemException e) {
            System.out.println("here3:   "+e.getMessage());
            //bean validation exception
        }
        catch(Exception e){
            System.out.println("here4:   "+e.getMessage());
            //throw new ClientImportServiceException(e, nextRecord.toString());
        }

        return client;
    }

    public void addClientToRepo(Client client, Level level){
        Client c = clientRepo.save(client);
        levelRepo.updateClientLevel4thColumn(level.getLevelId());
    }
}
