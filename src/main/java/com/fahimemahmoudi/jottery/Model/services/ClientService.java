package com.fahimemahmoudi.jottery.Model.services;


import com.fahimemahmoudi.jottery.Model.entities.Client;
import com.fahimemahmoudi.jottery.Repository.ClientRepository;
import com.fahimemahmoudi.jottery.Repository.LevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    private ClientRepository clientRepo;
    private ClientImportService clientImportService;
    private ClientValidator clientValidator;
    private LevelRepository clientLevelRepo;

    @Value("${clientList.path}")
    private String ClientListPath;

    @Autowired
    public ClientService(ClientRepository clientRepo, ClientValidator clientValidator,
                         LevelRepository clientLevelRepo, ClientImportService clientImportService) {

        this.clientRepo = clientRepo;
        this.clientValidator = clientValidator;
        this.clientLevelRepo = clientLevelRepo;
        this.clientImportService = clientImportService;
    }


    public ClientService(){
    }

//    @PostConstruct
//    @SneakyThrows
//    public void addClient(){
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//
//        try (Reader reader = Files.newBufferedReader(Paths.get(ClientListPath));
//             CSVReader csvReader = new CSVReader(reader)) {
//             String[] nextRecord;
//             while ((nextRecord = csvReader.readNext()) != null) {
//
//                 try {
//                     Client client = new Client(nextRecord[0], nextRecord[1], nextRecord[2], nextRecord[3],
//                                                        Integer.valueOf(nextRecord[4]), sdf.parse(nextRecord[5]),
//                                                        Gender.valueOf(nextRecord[6].toUpperCase()), nextRecord[7]);
//
//                     if (clientValidator.IsValidRecord(client)) {
//                          Long levelId = clientLevelRepo.getClientLevelIdByRange(client.getRate());
//                          Level cl = clientLevelRepo.getClientLevelById(levelId);
//                          client.setClientlevel(cl);
//                          clientRepo.save(client);
//                          clientLevelRepo.updateClientLevel4thColumn(cl.getLevelId());
//                     }
//                 }
//                 catch (ParseException e) {
//                     //parse date exception
//                 }
//                 catch (IllegalArgumentException | NullPointerException e) {
//                     //enum exceptions
//                 }
//                 catch (TransactionSystemException e) {
//                     //bean validation exception
//                 }
//                 catch (Exception e){
//                 }
//
//             }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    public void addClient()
    {
        clientImportService.init();
        while (clientImportService.hasNext()){
            Client client = clientImportService.addClient();
        }
    }

    public void truncateClients(){

        clientRepo.deleteAll();
    }
}
