package com.fahimemahmoudi.jottery.Model.services;

import com.fahimemahmoudi.jottery.Model.entities.Client;
import com.fahimemahmoudi.jottery.Model.others.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;


@Service
public class ClientValidator {

    private Boolean isValidRecord;

    @Autowired
    public ClientValidator() {
    }

    public Boolean IsValidRecord(Client client) {

        return
                validateClientCode(client.getClientCode()) &&
                validateIDNumber(client.getIDNumber()) &&
                validateBirthDay(client.getBirthDay()) &&
                validateGender(client.getGender()) &&
                validatePhoneNumber(client.getPhoneNum());
    }

    public final Boolean validateClientCode(String clientCode){

        return clientCode.matches("[0-9]+");
    }

    public final Boolean validateIDNumber(String IDNumber){

        return IDNumber.matches("[0-9]+");
    }

    public final Boolean validateBirthDay(Date birthDay){

        Calendar cal = Calendar.getInstance();
        cal.setTime(birthDay);
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH)+1;
        Integer day = cal.get(Calendar.DAY_OF_MONTH);


        return ((year>=1940 && year<=2019)
            && (month>=1 && month<=12)
            && (day>=1 && day <=31));
    }

    public final Boolean validateGender(Gender gender){

        return (gender.equals(Gender.valueOf("FEMALE")) ||
                (gender.equals(Gender.valueOf("MALE"))));
    }

    public final Boolean validatePhoneNumber(String phoneNum){

        return (phoneNum.startsWith("09")) && (phoneNum.length() ==11);
    }
}
