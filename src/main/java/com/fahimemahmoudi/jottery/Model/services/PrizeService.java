package com.fahimemahmoudi.jottery.Model.services;

import com.fahimemahmoudi.jottery.Model.entities.Level;
import com.fahimemahmoudi.jottery.Model.entities.Prize;
import com.fahimemahmoudi.jottery.Repository.LevelRepository;
import com.fahimemahmoudi.jottery.Repository.PrizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import java.util.List;

@Component
public class PrizeService {

        private LevelRepository clientLevelRepo;
        private PrizeRepository prizeRepo;

        @Autowired
        public PrizeService(LevelRepository clientLevelRepo,
                            PrizeRepository prizeRepo) {
            this.clientLevelRepo = clientLevelRepo;
            this.prizeRepo = prizeRepo;
        }

    public List<Prize> findAllPrizes(){
        return (List<Prize>) prizeRepo.findAll();
    }

    public Object addNewPrize (String prizeName, Integer prizeCount, Object prizeLevel){

        try {
            Level cl = clientLevelRepo.getClientLevelByName(prizeLevel.toString());
            Prize prize = new Prize(prizeName, prizeCount);
            prize.setClientlevel(cl);
            return prizeRepo.save(prize);
        } catch (TransactionSystemException e) {
        }
        catch (NullPointerException e) {
        }
        return null;
    }
}
