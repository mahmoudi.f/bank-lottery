package com.fahimemahmoudi.jottery.Model.services;


import com.fahimemahmoudi.jottery.Model.entities.Client;
import com.fahimemahmoudi.jottery.Model.entities.Level;
import com.fahimemahmoudi.jottery.Repository.ClientRepository;
import com.fahimemahmoudi.jottery.Repository.LevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import java.util.ArrayList;
import java.util.List;


@Component
public class LevelService {

    private LevelRepository clientLevelRepo;
    private ClientRepository clientDataRepo;

    private ClientService clientService;

    @Autowired
    public LevelService(LevelRepository clientLevelRepo,
                        ClientRepository clientDataRepo,
                        ClientService clientService) {
        this.clientLevelRepo = clientLevelRepo;
        this.clientDataRepo = clientDataRepo;
        this.clientService = clientService;
    }

    public List<Level> findAllLevels(){

        List<Level> results = new ArrayList<>();
        try {
            for (Level cl: (List<Level>) clientLevelRepo.findAll()) {
                results.add(Level.builder().levelName(cl.getLevelName())
                                                 .rangeStart(cl.getRangeStart())
                                                 .rangeEnd(cl.getRangeEnd())
                                                 .numberOfClients(cl.getNumberOfClients())
                                                 .build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    public Object addNewLevel(Level cl){

//        Integer count = clientDataRepo.getNumberOfClientsInRange(cl.getRangeStart(), cl.getRangeEnd());
        Integer count = clientDataRepo.countByRateGreaterThanEqualAndRateLessThan(cl.getRangeStart(), cl.getRangeEnd());
        cl.setNumberOfClients(count);
        try {
            Object obj = clientLevelRepo.save(cl);
            updateClientsLevel(cl);
            return obj;
        } catch (TransactionSystemException |
                DataIntegrityViolationException e) {
        }
        return null;
    }

    public void updateClientsLevel(Level level){

//        return clientDataRepo.save(Client.builder().clientlevel(levelId).build());
        List<Client> clients = clientDataRepo.findAlltByClientlevelNull();
        for (Client c:clients)
            if(level.getRangeStart() <= c.getRate() && level.getRangeEnd() > c.getRate())
            {
                c.setClientlevel(level);
                clientDataRepo.save(c);
            }
//        return clientDataRepo.updateClientsLevel(levelId, start, end);

    }

    public List<String> findAllClientLeveNames(){

        return clientLevelRepo.findAllClientLevelNames();
    }

}
