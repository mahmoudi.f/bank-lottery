package com.fahimemahmoudi.jottery.Model.services;


import com.fahimemahmoudi.jottery.Model.entities.Client;
import com.fahimemahmoudi.jottery.Model.entities.Lottery;
import com.fahimemahmoudi.jottery.Model.entities.Prize;
import com.fahimemahmoudi.jottery.Repository.ClientRepository;
import com.fahimemahmoudi.jottery.Repository.LotteryRepository;
import com.fahimemahmoudi.jottery.Repository.PrizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LotteryService {

    private LotteryRepository lotteryRepo;
    private ClientRepository clientDataRepo;
    private PrizeRepository prizeRepo;

    @Autowired
    public LotteryService(LotteryRepository lotteryRepo,
                          ClientRepository clientDataRepo,
                          PrizeRepository prizeRepo) {
        this.lotteryRepo = lotteryRepo;
        this.clientDataRepo = clientDataRepo;
        this.prizeRepo = prizeRepo;
    }

    public Object addNewLottery() {
        Lottery lottery = new Lottery();
        lottery.setClients(clientDataRepo.findAll());

        List<Prize> prizes = (List<Prize>) prizeRepo.findAll();
        lottery.setPrizes(prizes);
        Object obj = lotteryRepo.save(lottery);
        updateClientsLottery(lottery);
//        clientDataRepo.updateClientsLottery(lottery.getId());

        prizes.forEach(prize ->
        {
            prize.setLottery(lottery);
            prizeRepo.save(prize);
        });


//        prizeRepo.updatePrizesLottery(lottery.getId());
        return obj;
    }

    public void updateClientsLottery(Lottery lottery){

        List<Client> clients = clientDataRepo.findAllByLotteryNull();
        for (Client c:clients) {
            c.setLottery(lottery);
            try {
                clientDataRepo.save(c);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Lottery> findAllLotteries() {

        return (List<Lottery>) lotteryRepo.findAll();

    }

}