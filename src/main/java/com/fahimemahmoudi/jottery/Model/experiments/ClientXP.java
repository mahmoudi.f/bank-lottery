package com.fahimemahmoudi.jottery.Model.experiments;

import com.fahimemahmoudi.jottery.Model.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class ClientXP {

    @Value("${import.ClientList}")
    private Boolean importClients;
    private ClientService clientService;
    @Value("${wipe.database}")
    private Boolean wipeDB;

    @Autowired
    public ClientXP(ClientService clientService)
    {
        this.clientService = clientService;
    }

    @PostConstruct
    public void startup(){
        if(wipeDB)
            clientService.truncateClients();
        if(importClients) {
            clientService.addClient();
        }
    }

}
