package com.fahimemahmoudi.jottery.Model.entities;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;


@Entity @Table(name="prizes")
@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class Prize {

    @Id @GeneratedValue(strategy= GenerationType.AUTO)
    private Long prizeId;
//    @NonNull @NotBlank @Size(min=3,max=3)
//    @Column(unique = true) @Basic(fetch=FetchType.EAGER)
//    private String prizeCode;
    @NotBlank @Size(min=1, max=10) @Column(unique = true)
    private String prizeName;
    @NonNull @PositiveOrZero
    private Integer count;

    @NonNull @ManyToOne @JoinColumn(name="clientlevel")
    private Level clientlevel;

    @ManyToOne @JoinColumn(name="lottery")
    private Lottery lottery;

    public Prize(@NotBlank @Size(min = 1, max = 10) String prizeName,
                 @NonNull @PositiveOrZero @Max(5) Integer count
                 ) {

        this.prizeName = prizeName;
        this.count = count;
    }
}
