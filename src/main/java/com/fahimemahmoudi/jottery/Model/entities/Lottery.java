package com.fahimemahmoudi.jottery.Model.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity @Table(name="lotteries")
@Data @NoArgsConstructor @AllArgsConstructor
public class Lottery {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @OneToMany(mappedBy = "lottery", cascade = CascadeType.MERGE) @Basic(fetch = FetchType.LAZY)
    private List<Prize> prizes;

    @OneToMany(mappedBy = "lottery", cascade = CascadeType.MERGE) @Basic(fetch = FetchType.LAZY)
    private List<Client> clients;

    }


