package com.fahimemahmoudi.jottery.Model.entities;


import com.fahimemahmoudi.jottery.Model.others.Gender;
import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;
import javax.validation.constraints.*;


@Entity @Table(name="clients") @Component
@Data @NoArgsConstructor @AllArgsConstructor @Builder
public class Client {

    @Id @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @NotBlank @Size(min=9,max=9) @Column(unique = true) //@NotBlank
    private String clientCode;
    @NotBlank @Size(min=10,max=10) @Column(unique = true)
    private String IDNumber;
    @NotBlank  @Size(min=1, max=15)
    private String firstName;
    @NotBlank  @Size(min=1, max=20)
    private String lastName;
    @NonNull @Positive @Min(20) @Max(2000)
    private Integer rate;
    @Past
    private Date birthDay;
    @NonNull @Enumerated(EnumType.STRING)
    private Gender gender;
    @NotBlank @Size(min=3, max=12)
    private String phoneNum;

    @ManyToOne @JoinColumn(name="clientlevel")
    private Level clientlevel;

    @ManyToOne @JoinColumn(name="lottery")
    private Lottery lottery;

    public Client(@NotBlank @Size(min = 9, max = 9) String clientCode,
                  @NotBlank @Size(min = 10, max = 10) String IDNumber,
                  @NotBlank @Size(min = 1, max = 15) String firstName,
                  @NotBlank @Size(min = 1, max = 20) String lastName,
                  @NonNull @Positive @Min(20) @Max(2000) Integer rate,
                  @Past Date birthDay,
                  @NonNull Gender gender,
                  @NotBlank @Size(min = 3, max = 12) String phoneNum
                      ) {

        this.clientCode = clientCode;
        this.IDNumber = IDNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.rate = rate;
        this.birthDay = birthDay;
        this.gender = gender;
        this.phoneNum = phoneNum;
    }
}
