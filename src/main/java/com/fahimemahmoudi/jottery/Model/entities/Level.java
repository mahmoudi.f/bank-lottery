package com.fahimemahmoudi.jottery.Model.entities;


import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity @Table(name="levels") @Component
@Data @NoArgsConstructor @AllArgsConstructor @Builder
public class Level {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long levelId;
    @NonNull @Size(min=1, max=8) @Column(unique = true)
    private String levelName;
    @NonNull @Positive
    private Integer rangeStart;
    @NonNull @Positive
    private Integer rangeEnd;
    @NonNull @PositiveOrZero
    private Integer numberOfClients;

    @OneToMany(mappedBy = "clientlevel", cascade = CascadeType.ALL) @Basic(fetch = FetchType.LAZY)
    Set<Client> clients;

    @OneToMany(mappedBy = "clientlevel", cascade = CascadeType.ALL) @Basic(fetch = FetchType.LAZY)
    Set<Prize> prizes;



    public Level(@NonNull @Size(min = 1, max = 8) String levelName,
                 @NonNull @Positive Integer rangeStart,
                 @NonNull @Positive Integer rangeEnd,
                 @NonNull @PositiveOrZero Integer numberOfClients) {
        this.levelName = levelName;
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.numberOfClients = numberOfClients;
    }
}
