package com.fahimemahmoudi.jottery.Repository;

import com.fahimemahmoudi.jottery.Model.entities.Lottery;
import org.springframework.data.repository.CrudRepository;

public interface LotteryRepository extends CrudRepository<Lottery, Integer> {
}
