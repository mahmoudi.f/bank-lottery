package com.fahimemahmoudi.jottery.Repository;


import com.fahimemahmoudi.jottery.Model.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "select count(*) from clients where clients.rate >= :start and clients.rate < :end",
            nativeQuery = true)
    int getNumberOfClientsInRange(@Param("start") Integer start, @Param("end") Integer end);

    @Modifying
    @Query(value = "update clients set clients.clientlevel = :levelId where clients.rate >= :start and clients.rate < :end",
            nativeQuery = true)
    int updateClientsLevel(@Param("levelId") Long levelId, @Param("start") Integer start, @Param("end") Integer end);

    @Modifying
    @Query(value = "update clients set clients.lottery = :lotteryId",
            nativeQuery = true)
    int updateClientsLottery(@Param("lotteryId") Long lotteryId);

    Integer countByRateGreaterThanEqualAndRateLessThan(Integer start, Integer end);

    List<Client> findAlltByClientlevelNull();

    List<Client> findAllByLotteryNull();

}