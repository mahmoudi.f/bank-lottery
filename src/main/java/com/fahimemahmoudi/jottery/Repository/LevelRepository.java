package com.fahimemahmoudi.jottery.Repository;


import com.fahimemahmoudi.jottery.Model.entities.Level;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface LevelRepository extends CrudRepository<Level, String> {

    @Modifying
    @Query(value = "update levels as cl set cl.number_of_clients = cl.number_of_clients+1 where cl.level_id = :levelId",
            nativeQuery = true)
    int updateClientLevel4thColumn(@Param("levelId") Long levelId);

    @Query(value = "select level_id from levels where levels.range_start <= :rate and levels.range_end > :rate",
            nativeQuery = true)
    Long getClientLevelIdByRange(@Param("rate") Integer rate);

    @Query(value = "select * from levels where levels.level_id = :id",
            nativeQuery = true)
    Level getClientLevelById(@Param("id") Long id);

    @Query(value = "select * from levels where levels.level_name = :name",
            nativeQuery = true)
    Level getClientLevelByName(@Param("name") String name);

    @Query(value = "select level_name from levels",
            nativeQuery = true)
    List<String> findAllClientLevelNames();

    Level findByRangeEndAfterAndRangeStartBefore(Integer rate1, Integer rate2);

}

