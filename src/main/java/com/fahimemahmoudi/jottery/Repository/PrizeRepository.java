package com.fahimemahmoudi.jottery.Repository;

import com.fahimemahmoudi.jottery.Model.entities.Prize;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PrizeRepository extends CrudRepository<Prize, Integer> {

    @Modifying
    @Query(value = "update prizes set prizes.lottery = :lotteryId",
            nativeQuery = true)
    int updatePrizesLottery(@Param("lotteryId") Long lotteryId);

}
